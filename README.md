# TwRss

A Fiber web server for redirecting Twitter posts (but theoretically any webhook will work) to RSS. **This server does not itself reach out to Twitter for tweets.** Rather, this expects something else (in this case, IFTTT, but anything would do) to send traffic its way. This service just caches the tweets and serves them as RSS.

## Running

This has been set up to use Docker Compose. All you need to provide is the login keys, a home for the logs, a password for Redis and an optional custom username. An example configuration file is in `.env.example`:

```ini
LOG_PATH=""
KEY_PATH=""
REDIS_PASSWORD=""
USER_USERNAME=""
```

- `LOG_PATH` should point to any existing folder on your disk, preferrably an empty one. Logs will be created automatically when the application starts.
- `KEY_PATH` is where your **hashed** passwords for IFTTT and the user will live. More on that below.
- `REDIS_PASSWORD` is the password to Redis. It's not accessible outside of the Docker Compose environment, but still doesn't hurt to put something secure here.
- `USER_USERNAME` is the username you will use to access the RSS feed. If you leave it blank, the username will be `user`.

The application can be run with `docker compose up`. It will run on port `8080`, and can be accessed from there. At this point, I **HIGHLY** recommend using some reverse proxy (ie, Nginx) with some form of TLS. Letsencrypt takes like 2 minutes to set up. You're going to be sending base64 encoded passwords over the network. Please don't skip this.

### KEY_PATH: Generating login keys

The most involved part of this setup is generating your passwords. You'll need 2 of them: one for the `ifttt` user, and one for the `user` user (or whatever you set for `USER_USERNAME`). These should be **new passwords, not for any existing service (PLEASE)**, and need to be bcrypt hashed. You can hash them however you please, but here's how to do it in Python:

```python
import bcrypt
print(bcrypt.hashpw(b"(YOUR (hopefully) SUPER SECURE PASSWORD GOES HERE)", bcrypt.gensalt()).decode('ascii'))
```

Save the hashed IFTTT password to a file named `ifttt` and the hashed user password to a file named `user` in the same folder (preferrably somewhere safe). Then enter that for the `KEY_PATH`.

One last thing, you'll need the IFTTT account's Authorization header. If you know how to generate this, then feel free to do so however works best for you, you HTTP guru. For everyone else, Python's back to the rescue:

```python
import base64
print(base64.b64encode(b"ifttt:(THAT SUPER SECRET PASSWORD AGAIN)").decode('ascii'))
```

Your header will be `Authorization: Basic (the base64 encoded string)`. For example: `Authorization: Basic aWZ0dHQ6aGVsbG8gYzo=`. Hold onto that, you'll need it in a sec.

## Adding posts

### Pointing IFTTT here

Once you've deployed the application and have confirmed it's not on fire (you can check with the `/count` endpoint), now you can configure whatever service you want to call Twrss. You don't have to use IFTTT, but it's my weapon of choice. One major downside is that you will need IFTTT Pro for it, which is a (relatively small) fee, and even that only gets you 20 applets (one applet per user, thanks IFTTT).

If you do decide to use IFTTT, here's the formula:
- **IF** New tweet by user @whoever,
- **THEN** Make a web request:
  - URL: `https://your-server-here.com/add`
  - Method: `POST`
  - Content Type: `application/json`
  - Additional Headers: `(your header from the key setup above ("Authorization: Basic (blah blah blah)"))`
  - Body: `{"body": "<<<{{Text}}>>>", "created": "{{CreatedAt}}", "link": "{{LinkToTweet}}", "author": "{{UserName}}"}`

### Pointing literally anything else here

You don't need to use IFTTT to send tweets over. As long as it can post JSON and do Authorization, it can interface with this. Here's the JSON format expected:

```json
{
  "body": "(the message body)",
  "created": "(time in the amazingly inconvenient format of 'January 03, 2023 at 03:04PM')",
  "link": "(any link at all)",
  "author": "(whatever key name you want to access the posts with later)"
}
```

It will reply with the number of records for that user, if all goes well.

## Getting records

Any RSS reader that supports authentication can retrieve records from here. I personally like Liferea on Linux, and it handles it flawlessly. Simply point your RSS reader to `http://your-server-here.com/get?user=(the user you want to watch)` and it will do the rest. Liferea will prompt you for your username and password (specified during setup). Just enter that, and you'll be golden.

### Suppress RTs

Following someone who retweets a lot? Same. Just add the `hideRts=true` query parameter to the `get` URL to filter them out.

### Suppress replies

Following someone who gets into a lot of arguments on Twitter? Also same. Just add the `hideReplies=true` query parameter to the `get` URL to filter those out as well. You can use both `hideRts` and `hideReplies` at the same time if you want.

## Clearing everything

You shouldn't encounter any issues with errant records causing the server to trip up, but you *might* encounter some memory issues if the server has been on for a super long time and collected a ton of tweets. If this happens, hit the `/clear` endpoint with a `DELETE` request (Postman is probably your best bet on doing this), and the entire Redis cache will be cleared. I highly recommend pulling records to your RSS reader before doing this, so you don't miss out on anything.
