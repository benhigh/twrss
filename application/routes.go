package main

import (
	"fmt"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gorilla/feeds"
)

func add(c *fiber.Ctx) error {
	t := new(Tweet)
	if err := c.BodyParser(t); err != nil {
		printLog(c, fmt.Sprintf("New add request failed body parsing: %s", err.Error()))
		return c.SendStatus(400)
	}

	feedsList, err := getForUser(c, t.Author)
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	created, err := time.Parse(DATE_PARSE_FORMAT, t.Created)
	if err != nil {
		printLog(c, fmt.Sprintf("Time value \"%s\" couldn't be parsed. Using now for tweet \"%s\" instead.", t.Created, t.Link))
		created = time.Now()
	}

	feedsList = append(feedsList, &feeds.Item{
		Title: t.Body,
		Author: &feeds.Author{
			Name: t.Author,
		},
		Description: t.Body,
		Created:     created,
		Link: &feeds.Link{
			Href: t.Link,
		},
	})

	setForUser(c, t.Author, feedsList)
	printLog(c, fmt.Sprintf("Added tweet for %s: %s", t.Author, t.Link))
	return c.SendString(fmt.Sprintf("%d", len(feedsList)))
}

func get(c *fiber.Ctx) error {
	user := strings.Trim(c.Query("user"), " ")
	if user == "" {
		return c.SendStatus(fiber.StatusBadRequest)
	}

	items, err := getForUser(c, user)
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	feed := &feeds.Feed{
		Title:       fmt.Sprintf("@%s (Twitter)", user),
		Link:        &feeds.Link{Href: fmt.Sprintf("https://twitter.com/%s", user)},
		Description: fmt.Sprintf("Tweets from user @%s", user),
	}

	feed.Items = items
	if c.Query("hideRts", "") == "true" {
		feed.Items = filterOutTweetStartsWith(feed.Items, "RT @")
	}
	if c.Query("hideReplies", "") == "true" {
		feed.Items = filterOutTweetStartsWith(feed.Items, "@")
	}

	res, err := feed.ToRss()
	if err != nil {
		printLog(c, fmt.Sprintf("Failed to generate RSS feed: %s", err.Error()))
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	c.Response().Header.Add("Content-Type", "application/rss+xml")
	return c.SendString(res)
}

func counts(c *fiber.Ctx) error {
	keys, err := getAllKeys(c)
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	details := make(map[string]int)

	for _, e := range keys {
		items, err := getForUser(c, e)
		if err != nil {
			return c.SendStatus(fiber.StatusInternalServerError)
		}
		details[e] = len(items)
	}

	return c.JSON(details)
}

func clear(c *fiber.Ctx) error {
	err := flushAll(c)
	if err != nil {
		printLog(c, fmt.Sprintf("** ERROR: Failed to flush all in Redis: %s", err.Error()))
		return c.SendStatus(fiber.StatusInternalServerError)
	}
	return c.SendStatus(fiber.StatusOK)
}
