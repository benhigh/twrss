package main

const ACCESS_LOG_PATH = "/etc/twrss/logs/access.log"
const APP_LOG_PATH = "/etc/twrss/logs/twrss.log"

const DISABLE_AUTH_ENV_VAR = "DISABLE_AUTH"
const LOG_CONSOLE_ENV_VAR = "LOG_CONSOLE"

const IFTTT_KEY_PATH_ENV_VAR = "IFTTT_KEY_PATH"
const USER_KEY_PATH_ENV_VAR = "USER_KEY_PATH"

const DEFAULT_IFTTT_KEY_PATH = "/etc/twrss/keys/ifttt"
const DEFAULT_USER_KEY_PATH = "/etc/twrss/keys/user"

const IFTTT_USERNAME = "ifttt"

const USER_USERNAME_ENV_VAR = "USER_USERNAME"
const DEFAULT_USER_USERNAME = "user"

const LOG_FORMAT string = "${locals:requestid} | ${time} | ${status} | ${latency} | ${header:X-Forwarded-For} | ${method} | ${path}?${queryParams} | ${locals:username}\n"

const DATE_PARSE_FORMAT = "January _2, 2006 at 03:04PM"

const REDIS_HOST_ENV_VAR = "REDIS_HOST"
const REDIS_PASSWORD_ENV_VAR = "REDIS_PASSWORD"

const DEFAULT_REDIS_HOST = "redis"
