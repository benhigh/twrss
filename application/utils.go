package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/gorilla/feeds"
)

func printLog(c *fiber.Ctx, msg string) {
	rid := fmt.Sprintf("%v", c.Locals("requestid"))
	log.Printf("| %s | %s\n", rid, msg)
}

func loadPasswordHash(path string) string {
	pwd, err := os.ReadFile(path)
	if err != nil {
		log.Panicf("Failed to load key at \"%s\": %s", path, err.Error())
	}
	log.Printf("Opened password hash at %s\n", path)
	return string(pwd)
}

func openLogFile(path string) *os.File {
	file, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Panicf("Failed to open logfile at \"%s\": %s", path, err.Error())
	}
	return file
}

func getEnvOrElse(name string, defaultValue string) string {
	value := os.Getenv(name)
	if value == "" {
		return defaultValue
	}
	return value
}

func filterOutTweetStartsWith(tweets []*feeds.Item, startsWith string) []*feeds.Item {
	filtered := make([]*feeds.Item, 0)
	for _, tw := range tweets {
		if !strings.HasPrefix(tw.Description, startsWith) {
			filtered = append(filtered, tw)
		}
	}
	return filtered
}
