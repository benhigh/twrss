package main

import (
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/basicauth"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/requestid"
	"github.com/redis/go-redis/v9"
	"golang.org/x/crypto/bcrypt"
)

var redisClient *redis.Client

func main() {
	accessLog := os.Stdout
	if getEnvOrElse(LOG_CONSOLE_ENV_VAR, "false") == "false" {
		appLog := openLogFile(APP_LOG_PATH)
		defer appLog.Close()
		log.SetOutput(appLog)
		accessLog = openLogFile(ACCESS_LOG_PATH)
		defer accessLog.Close()
		log.Printf("Opened access log %s\n", ACCESS_LOG_PATH)
	} else {
		log.Println("** WARNING: Using stdout for logging. Only use this in development (please).")
	}

	redisClient = connectRedis()

	app := fiber.New()
	app.Use(requestid.New())
	app.Use(logger.New(logger.Config{
		Output: accessLog,
		Format: LOG_FORMAT,
	}))

	if getEnvOrElse(DISABLE_AUTH_ENV_VAR, "false") == "false" {
		username := getEnvOrElse(USER_USERNAME_ENV_VAR, DEFAULT_USER_USERNAME)
		users := map[string]string{
			IFTTT_USERNAME: loadPasswordHash(getEnvOrElse(IFTTT_KEY_PATH_ENV_VAR, DEFAULT_IFTTT_KEY_PATH)),
			username:       loadPasswordHash(getEnvOrElse(USER_KEY_PATH_ENV_VAR, DEFAULT_USER_KEY_PATH)),
		}
		app.Use(basicauth.New(basicauth.Config{
			Users: users,
			Authorizer: func(uname string, pwd string) bool {
				hashed, exists := users[uname]
				if !exists {
					return false
				}

				return bcrypt.CompareHashAndPassword([]byte(hashed), []byte(pwd)) == nil
			},
		}))
	} else {
		log.Println("** WARNING: Running with authentication disabled. Only use this in development (pretty please).")
	}

	app.Post("/add", add)
	app.Get("/get", get)
	app.Get("/counts", counts)
	app.Delete("/clear", clear)

	log.Println("Initialized application. Listening on port 8080.")

	app.Listen(":8080")
}
