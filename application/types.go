package main

type Tweet struct {
	Body    string `json:"body"`
	Created string `json:"created"`
	Link    string `json:"link"`
	Author  string `json:"author"`
}
