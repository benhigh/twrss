package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gorilla/feeds"
	"github.com/redis/go-redis/v9"
)

func connectRedis() *redis.Client {
	address := fmt.Sprintf("%s:6379", getEnvOrElse(REDIS_HOST_ENV_VAR, DEFAULT_REDIS_HOST))
	password := getEnvOrElse(REDIS_PASSWORD_ENV_VAR, "")
	if password == "" {
		log.Panicln("Redis password cannot be blank or missing!")
	}

	return redis.NewClient(&redis.Options{
		Addr:     address,
		Password: password,
		DB:       0,
	})
}

func getForUser(c *fiber.Ctx, username string) ([]*feeds.Item, error) {
	exists, err := redisClient.Exists(c.Context(), username).Result()
	if err != nil {
		printLog(c, fmt.Sprintf("** ERROR: Failed to check if username exists in Redis for username %s: %s", username, err.Error()))
		return nil, err
	}

	if exists == 0 {
		return []*feeds.Item{}, nil
	}

	value, err := redisClient.Get(c.Context(), username).Result()
	if err != nil {
		printLog(c, fmt.Sprintf("** ERROR: Failed to retrieve result from Redis for user %s: %s", username, err.Error()))
		return nil, err
	}

	var results []*feeds.Item
	err = json.Unmarshal([]byte(value), &results)
	if err != nil {
		printLog(c, fmt.Sprintf("** ERROR: Failed to unmarshal result from Redis for user %s: %s", username, err.Error()))
		return nil, err
	}

	return results, nil
}

func setForUser(c *fiber.Ctx, username string, feeds []*feeds.Item) error {
	marshalled, err := json.Marshal(feeds)
	if err != nil {
		printLog(c, fmt.Sprintf("** ERROR: Failed to marshal for username %s: %s", username, err.Error()))
		return err
	}

	err = redisClient.Set(c.Context(), username, string(marshalled), 0).Err()
	if err != nil {
		printLog(c, fmt.Sprintf("** ERROR: Failed to save to Redis for username %s: %s", username, err.Error()))
		return err
	}

	return nil
}

func getAllKeys(c *fiber.Ctx) ([]string, error) {
	res, err := redisClient.Keys(c.Context(), "*").Result()
	if err != nil {
		printLog(c, fmt.Sprintf("** ERROR: Failed to get all keys: %s", err.Error()))
		return nil, err
	}

	return res, nil
}

func flushAll(c *fiber.Ctx) error {
	return redisClient.FlushAll(c.Context()).Err()
}
